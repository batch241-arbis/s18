/*
	You can directly pass date into the function. The function can then call/use that data which is referred as "name" within the parameter
*/

/*
	Syntax:
	function functionName(paramter) {
		code block
	}
	function(argument)
*/

//Name is called parameter
//"parameter" acts as a named variable/container that exists only inside of a function
//It is used to store information that is provided to a function when it is called/invoked
function printName(name){
	console.log("My name is " + name);
};

//"Juan" and "Miah", the information/data provided directly into the function, it is called an argument
printName("Juana");
printName("Rafhael");

//Variables can also be passed as an ARGUMENT
let userName = "Elaine";
printName(userName);

//Using Multiple Parameters
function createFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Erven", "Joshua", "Cabral");
//"Erven" will be stored in the parameter "firstName"
//"Joshua" will be stored in the parameter "middleName"
//"Cabral" will be stored in the parameter "lastName"

createFullName("Eric","Andales");
createFullName("Roland","John","Doroteo","Jose");

/*
	Mini-Activtiy:
		1. Create a function which is able to receive data as an argument.
			-This function should be able to receive the name of your favorite superhero
			-Display the name of your favorite superhero in the console
*/

function faveHero(heroName){
	console.log("Your favorite superhero is " + heroName);
}

// let hero = prompt("Put your favorite superhero here:");
// faveHero(hero);

//Return Statement
/*
	The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function
*/

function returnFullName(firstName, middleName, lastName){
	return firstName + " " + middleName + " " + lastName;
}

//Whatever value that is returned from the "returnFullName" function can be stored in a variable

let completeName = returnFullName("Rafhael", "Danniel", "Arbis");
console.log(completeName);


function returnAddress(city, country){
	let fullAddress = city + ", " + country;
	return fullAddress;
}

let myAddress = returnAddress("Caloocan", "Philippines");
console.log(myAddress);

function printPlayerInformation(userName, level, job) {
	let playerInformation = "Username: " + userName + "\n" + "Level: " + level + "\n" + "Job: " + job;
	return playerInformation
}

let fullPlayerInformation = printPlayerInformation("Rafhael", 25, "Mage");
console.log(fullPlayerInformation);