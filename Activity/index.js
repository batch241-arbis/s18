console.log("Hello World");

function addNumber(variableA,variableB){
	let sum = variableA + variableB;
	console.log("Displayed sum of " + variableA + " and " + variableB + " is:");
	console.log(sum);
}

addNumber(5,15);

function subtractNumber(variableA,variableB){
	let difference = variableA - variableB;
	console.log("Displayed difference of " + variableA + " and " + variableB + " is:");
	console.log(difference);
}

subtractNumber(20,5);

function multiplyNumber(variableA,variableB){
	let product = variableA * variableB;
	return product;
}

let prodAnswer = multiplyNumber(50,10);
console.log("The product of 50 and 10 is:");
console.log(prodAnswer);

function divideNumber(variableA,variableB){
	let quotient = variableA / variableB;
	return quotient;
}

let quotAnswer = divideNumber(50,10);
console.log("The quotient of 50 and 10 is:");
console.log(quotAnswer);

function getArea(radius) {
	let area = 3.14*(radius**2);
	return area;
}

let circleArea = getArea(15);
console.log("The result of getting the area of a circle with 15 radius:");
console.log(circleArea);

function getAverage(varA,varB,varC,varD) {
	let average = (varA + varB + varC + varD)/4
	return average;
}

let averageVar = getAverage(20,40,60,80);
console.log("The average of 20, 40, 60 and 80:");
console.log(averageVar);

function getGrades(scoreA,scoreB) {
	let grades = ((scoreA*2) + (scoreB*2))/2;
	let isPassed = grades >= 75
	return isPassed;
}

let isPassingScore = getGrades(38,50);
console.log("Is 38/50 a passing score?");
console.log(isPassingScore);